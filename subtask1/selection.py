from formula import Formula
import math
import statsmodels.formula.api as smf


def backward_selection(data, response):
    """
    data.. data frame with explanatory variables and response as columns
    response.. name of the column whose value should be explained

    returns model fitted to the data by backward selection
    """
    main_effects = {col for col in data.columns if col != response}

    formula = full_model_for(response, main_effects)

    best_aic_score = highest_round_aic_score = math.inf

    while True:
        scores_candidates = dict()
        for effect in main_effects:
            # apply modification
            formula.remove_main_effect(effect)

            # rate
            f = str(formula)
            print(f"Trying model without: {effect}")
            scores_candidates[effect] = smf.logit(f, data).fit(disp=0).aic

            # revert modification
            formula.add_main_effect(effect)
            formula.add_all_interactions()

        worst_candidate, highest_round_aic_score = max_items(scores_candidates)

        if highest_round_aic_score < best_aic_score:
            main_effects.remove(worst_candidate)

            best_aic_score = highest_round_aic_score
        else:
            break

    return smf.logit(str(formula), data).fit(disp=0)


def full_model_for(target, effects):
    f = Formula(target, *effects)
    f.add_all_interactions()
    return f


def max_items(dictionary):
    max_key = max(dictionary, key=dictionary.get)
    return max_key, dictionary[max_key]


def test_max_of_dict():
    d = {"a": 3, "b": 2}
    key, val = max_items(d)
    assert key == "a"
    assert val == 3
