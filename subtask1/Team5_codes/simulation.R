
simulation_mt <- function(data,selected_model, parameterlist,num_iterations=4, num_pseudo_obs=200) {
    summarylist_data.glm.back = summary(selected_model)$coef
    #print(summarylist_data.glm.back)
    # Simulation loop

    # Problem for small sample size ~ 50 : 

    #Warning message:
    #"glm.fit: fitted probabilities numerically 0 or 1 occurred"

    # Sample size is smaller than 50: The algorithm does not converge

    # but it becomes better with larger samplezise
    results <-list()
    sim_res <-list()
    mle_mu<-list()
    mle_sd<-list()
    newdesignmatrix = data[sample(nrow(data), 50), ]
    #head(newdesignmatrix)
    numberofloops = num_iterations
    k = 1

         # start simulation
         set.seed(1)
         #number pseudo observations
         simanz <- num_pseudo_obs

         #number coeffizients
         m = length(coef(selected_model))
        

         # matrix to save parameters, that are estimated for the different pseudoobservations
        parameter = matrix(0, simanz, m)
    msgInfo<-sprintf("Simulating %i pseudo observations for %i iterations with increment of 50 samples...", simanz,numberofloops)
    print(msgInfo)
    while (k <=numberofloops)
         {
         # estimate parameter for the data of design matrix
         newmod_newdesignmatrix <- glm(mt.presence ~1+dis.isl.MT  + dist.land + no.hab + dens.ramet + dens.group+ dis.isl.MT:dist.land, data=newdesignmatrix, family=binomial(link="logit") )

         # calculate inverse fisher information
         inv_Fisher_inf = vcov(newmod_newdesignmatrix)
         #print(inv_Fisher_inf)

         # list for datas of the model
         summarylist_newdesignmatrix =  summary(newmod_newdesignmatrix)$coef
         #print(summarylist_newdesignmatrix)
         #print(summarylist_newdesignmatrix[1,1])

         # number of rows of designmatrix
         n <- nrow(newdesignmatrix)
         #print(n)

         # calculate expaction value (assume the estimated parameters from the origninal model as true parameter, 
         # because we assume that the model of the original data is the true model
         mu <- predict(selected_model, type="response", newdata=newdesignmatrix)
         #print(mu)
        
         for (i in 1:simanz)
             {
             ### Simulation of pseudo observations
             y.sim <- rbinom(n,1, prob = mu)
             ### estimation parameters
             newmod <- glm(y.sim~1+dis.isl.MT  + dist.land + no.hab + dens.ramet + dens.group+ dis.isl.MT:dist.land, data=newdesignmatrix, family=binomial(link="logit") )
             ### storage parameters 
             summarylist =  summary(newmod)$coef
                for (p in 1:m){
                    parameter[i,p] = summarylist[p,1]
                }
            }
        
         sim_res[[k]] = parameter
        
         # plot distribution for [,1], that means the intercept parameter 
         #options(repr.plot.width = 10, repr.plot.height = 5)
        
         msgInfo<-sprintf("NEW SIMULATION WITH SAMPLESIZE %i",n)
         print(msgInfo)
         for (i in 1:m){       
            mu <-summarylist_data.glm.back[i,1]
            sigma<-sqrt(inv_Fisher_inf[i,i])
            mle_mu[[i]] = mu
            mle_sd[[i]] = sigma
            #print_mu<-sprintf("mean: %.10f",mu)
            #print(print_mu)
            #print_sd<-sprintf("sd: %.10f",sigma)
            #print(print_sd)
            
            par(mfrow = c(1, 3))
            #hist(parameter[,i], freq = F, main = paste("Hist- Plot von Parameter \n", parameterlist[i]), xlab = paste("beta",parameterlist[i]) )
                    
            #z <- seq(min( unlist(parameter[,i]) ) -  10*inv_Fisher_inf[i,i],max(unlist(parameter[,i]) ) + 10*inv_Fisher_inf[i,i] ,inv_Fisher_inf[i,i])  
            
            # for normal distribution we use the intercept parameter of the newdesignmatix model und for the standard deviation we use sqrt(inv_Fisher_inf[1,1]),
            #i.e. the diagonal entries of the inverse fisher information matrix, because that are the varianzes

            #points(z, dnorm(z, mean = mu , sd =sigma ), type = "l", col = 3)
            
            #Empirical cumulative distribution plot
            #plot(ecdf(parameter[,i]), main = paste("ECDF- Plot von Parameter \n", parameterlist[i]), xlab = paste("beta",parameterlist[i]))
            #points(z, pnorm(z,  mean = mu, sd = sigma), type = "l", col = 3)

            #qq plot - examine how well the normal distribution ist: the poits should lay on the line
            #qqnorm(parameter[,i], main = paste("Q - Q - Plot von  Parameter \n", parameterlist[i]), xlab = paste("beta",parameterlist[i]))
            #qqline(parameter[,i])
        }
        
         k = k+1
         toaddrows = data[sample(nrow(data), 50), ]

         newdesignmatrix<- rbind(newdesignmatrix , toaddrows)

    }
    print("Done...")
    results[["sim_results"]] = sim_res
    results[["mle_mu"]] = mle_mu
    results[["mle_sd"]] = mle_sd
    return(results)
}



simulation_mf <- function(data,selected_model, parameterlist,num_iterations=4, num_pseudo_obs=200) {
    summarylist_data.glm.back = summary(selected_model)$coef
    #print(summarylist_data.glm.back)
    # Simulation loop

    # Problem for small sample size ~ 50 : 

    #Warning message:
    #"glm.fit: fitted probabilities numerically 0 or 1 occurred"

    # Sample size is smaller than 50: The algorithm does not converge

    # but it becomes better with larger samplezise
    results <-list()
    sim_res <-list()
    mle_mu<-list()
    mle_sd<-list()
    newdesignmatrix = data[sample(nrow(data), 50), ]
    #head(newdesignmatrix)
    numberofloops = num_iterations
    k = 1

         # start simulation
         set.seed(1)
         #number pseudo observations
         simanz <- num_pseudo_obs

         #number coeffizients
         m = length(coef(selected_model))
        

         # matrix to save parameters, that are estimated for the different pseudoobservations
        parameter = matrix(0, simanz, m)
    msgInfo<-sprintf("Simulating %i pseudo observations for %i iterations with increment of 50 samples...", simanz,numberofloops)
    print(msgInfo)
    while (k <=numberofloops)
         {
         # estimate parameter for the data of design matrix
 
         newmod_newdesignmatrix <- glm(mf.presence ~1+dist.isl.MF + mean.height + dist.land + no.hab + dens.ramet + dens.group + dist.isl.MF:dens.ramet + dens.ramet:dens.group+ dist.land:dens.ramet + dist.land:dens.group + mean.height:dens.group + mean.height:dens.ramet, data=newdesignmatrix, family=binomial(link="logit") )

         # calculate inverse fisher information
         inv_Fisher_inf = vcov(newmod_newdesignmatrix)
         #print(inv_Fisher_inf)

         # list for datas of the model
         summarylist_newdesignmatrix =  summary(newmod_newdesignmatrix)$coef
         #print(summarylist_newdesignmatrix)
         #print(summarylist_newdesignmatrix[1,1])

         # number of rows of designmatrix
         n <- nrow(newdesignmatrix)
         #print(n)

         # calculate expaction value (assume the estimated parameters from the origninal model as true parameter, 
         # because we assume that the model of the original data is the true model
         mu <- predict(selected_model, type="response", newdata=newdesignmatrix)
         #print(mu)
        
         for (i in 1:simanz)
             {
             ### Simulation of pseudo observations
             y.sim <- rbinom(n,1, prob = mu)
             ### estimation parameters
             newmod <- glm(y.sim~1+dist.isl.MF + mean.height + dist.land + no.hab + dens.ramet + dens.group + dist.isl.MF:dens.ramet + dens.ramet:dens.group + dist.land:dens.ramet + dist.land:dens.group + mean.height:dens.group + mean.height:dens.ramet, data=newdesignmatrix, family=binomial(link="logit") )
             ### storage parameters 
             summarylist =  summary(newmod)$coef
                for (p in 1:m){
                    parameter[i,p] = summarylist[p,1]
                }
            }
        
         sim_res[[k]] = parameter
        
         # plot distribution for [,1], that means the intercept parameter 
         #options(repr.plot.width = 10, repr.plot.height = 5)
        
         msgInfo<-sprintf("NEW SIMULATION WITH SAMPLESIZE %i",n)
         print(msgInfo)
         for (i in 1:m){       
            mu <-summarylist_data.glm.back[i,1]
            sigma<-sqrt(inv_Fisher_inf[i,i])
            mle_mu[[i]] = mu
            mle_sd[[i]] = sigma
            #print_mu<-sprintf("mean: %.10f",mu)
            #print(print_mu)
            #print_sd<-sprintf("sd: %.10f",sigma)
            #print(print_sd)
            
            par(mfrow = c(1, 3))
            #hist(parameter[,i], freq = F, main = paste("Hist- Plot von Parameter \n", parameterlist[i]), xlab = paste("beta",parameterlist[i]) )
                    
            #z <- seq(min( unlist(parameter[,i]) ) -  10*inv_Fisher_inf[i,i],max(unlist(parameter[,i]) ) + 10*inv_Fisher_inf[i,i] ,inv_Fisher_inf[i,i])  
            
            # for normal distribution we use the intercept parameter of the newdesignmatix model und for the standard deviation we use sqrt(inv_Fisher_inf[1,1]),
            #i.e. the diagonal entries of the inverse fisher information matrix, because that are the varianzes

           # points(z, dnorm(z, mean = mu , sd =sigma ), type = "l", col = 3)
            
            #Empirical cumulative distribution plot
            #plot(ecdf(parameter[,i]), main = paste("ECDF- Plot von Parameter \n", parameterlist[i]), xlab = paste("beta",parameterlist[i]))
            #points(z, pnorm(z,  mean = mu, sd = sigma), type = "l", col = 3)

            #qq plot - examine how well the normal distribution ist: the poits should lay on the line
            #qqnorm(parameter[,i], main = paste("Q - Q - Plot von  Parameter \n", parameterlist[i]), xlab = paste("beta",parameterlist[i]))
            #qqline(parameter[,i])
        }
        
         k = k+1
         toaddrows = data[sample(nrow(data), 50), ]

         newdesignmatrix<- rbind(newdesignmatrix , toaddrows)

    }
    print("Done...")
    results[["sim_results"]] = sim_res
    results[["mle_mu"]] = mle_mu
    results[["mle_sd"]] = mle_sd
    return(results)
}




