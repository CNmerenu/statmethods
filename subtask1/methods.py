#!/usr/bin/env python
# coding: utf-8

# In[1138]:


import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt 
import seaborn as sb 
import numpy as np
import random
import math
from scipy.stats import norm
sb.set() #using seaborn style for all plots 
sb.set_theme(color_codes=True)
import pandas as pd 
import warnings
warnings.filterwarnings("ignore")


# In[369]:


def remove_dot(input, c1='.',c2='_'): 
     # create lambda to replace c1 with c2, 
    newChars = map(lambda x: x if (x!=c1 and x!=c2) else c1 if (x==c2) else c2,input) 
    return (''.join(newChars)) 

def logistic_function(p):
    f = np.exp(p) / (1 + np.exp(p))
    return f


# In[4]:


def model_summary_to_dataframe(model):
    '''take the result of an statsmodel results table and transforms it into a dataframe'''
    pvals = model.pvalues
    coeff = model.params
    conf_lower = model.conf_int()[0]
    conf_higher = model.conf_int()[1]
    significance = []
    for pval in pvals:
        if pval <= 0.05:
            significance.append("Yes")
        else:
            significance.append("No")

    results_df = pd.DataFrame({"pvals":pvals,
                               "coeff":coeff,
                               "conf_lower":conf_lower,
                               "conf_higher":conf_higher,
                               "significance":significance
                                })

    #Reordering...
    results_df = results_df[["coeff","pvals","conf_lower","conf_higher", "significance"]]
    
    return results_df


# In[5]:


def forward_selection(data, response):
    """Linear model designed by forward selection.

    Parameters:
    -----------
    data : pandas DataFrame with all possible predictors and response

    response: string, name of response column in data

    Returns:
    --------
    model: an "optimal" fitted statsmodels linear model
           with an intercept
           selected by forward selection
           evaluated by adjusted AIC 
    """
    remaining = set(data.columns)
    remaining.remove(response)
    selected = []
    current_aic_score = lowest_aic_score = math.inf
    print("Start of search ...")
    while remaining and current_aic_score == lowest_aic_score:
        #scores_with_candidates = []
        scores_candidates = {}
        for candidate in remaining:
            combo = ' + '.join(selected + [candidate])
            print("Trying model with predictor: {}".format(combo))
            formula = "{} ~ {} + 1".format(response,combo)
            aic_score = smf.logit(formula, data).fit(disp=0).aic
            print(f"AIC score: is {aic_score}")
            scores_candidates[aic_score] = candidate
        #print(f"the scores and candidates: for this round : {scores_candidates}")
        list_aic=list(scores_candidates.keys())
        lowest_aic_score = sorted(list_aic,reverse=True).pop()
        best_candidate = scores_candidates[lowest_aic_score]
        print(f"Best Case for this round: Score : {lowest_aic_score} and candidate: {best_candidate}")
        if  lowest_aic_score < current_aic_score:
            remaining.remove(best_candidate)
            selected.append(best_candidate)
            print(f"select candidate added: {best_candidate}")
            current_aic_score = lowest_aic_score
            
    select_combo = ' + '.join(selected)   
    
    print(f"End of search.. \nApplying the selected predictors with lowest AIC score: {select_combo}" )
    
    formula = "{} ~ {} + 1".format(response,select_combo)
                                   
    
    model = smf.logit(formula, data).fit(disp=0)
    print(f"Final model returned: AIC: {model.aic}")
    print(model.summary())
    print("Summarized Report")
    model_result_df = model_summary_to_dataframe(model)
    
    print(model_result_df)
    return model


# In[6]:


def check_VIF(X_df):
    X_df=sm.add_constant(X_df)
    vif = pd.DataFrame()
    vif["VIF Factor"] = [variance_inflation_factor(X_df.values, i) for i in range(X_df.shape[1])]
    vif["features"] = X_df.columns
    print(vif.round(1))
    


# In[7]:


#First we load in the dataset 
data = pd.read_csv('islands.csv',encoding = 'ISO-8859-1')
#and take a peek in 
data.head()


# In[8]:


#we will remove the Name column as it clearly doesn't add much value for the prediction task
data = data.drop(['Name'], axis=1)
data.dropna() ## removes not availables
data.head()


# In[9]:


#Number of entries in dataset
num_entries = len(data)
print(f"Number of entries in dataset: {num_entries}")
features = list(data)[:-2]
outcome_variables = list(data)[-2:]
outcome_mt = outcome_variables[0]
outcome_mf = outcome_variables[1]
print(f"Independent variables (predictors): {features} \n")
print(f"Dependent variable 1: {outcome_mt} \n")
print(f"Dependent variable 2: {outcome_mf}\n")

X = data[features]
Ymt = data[outcome_mt]
Ymf = data[outcome_mf]


# * Visualizing the raw data as first step to analysis 
# 

# In[10]:


# Data distribution for the MT presence outcome variable 

fig, axes = plt.subplots(1, 2, sharex=True, figsize=(10,5))
fig.suptitle('Distribution of outcome variables')
axes[0].set_title('MF presence count ')
axes[1].set_title('MT presence count ')

sb.countplot(ax=axes[0], x=outcome_mf, data=data)
sb.countplot(ax=axes[1], x=outcome_mt, data=data)

plt.show()


# In[429]:


# Data distribution of the feature variables (predictors): 
sb.pairplot(data=X)
plt.show()


# * For good predictions, it is essential to include the good independent variables (features) for fitting the model (e.g. variables that are not highly correlated). If we include all features, there is a great chance of including variables that have influences on each other, leading to multicollinearity. 
# * Let’s visualize the data for correlation among the independent variables using a heatmap using the pearson correlation coefficient

# In[11]:


pearsoncorr = X.corr(method='pearson')
sb_plot = sb.heatmap(pearsoncorr,xticklabels=pearsoncorr.columns,yticklabels=pearsoncorr.columns,cmap='RdBu_r',annot=False,linewidth=0.5)
plt.title("Correlation HeatMap")
plt.show()


# In[12]:


check_VIF(X)


# In[13]:


#We can see that there is multicollinearity in our dataset, 
# dis.isl.MT is strongly correlated to that dis.isl.MF. We will then separate the feature dataset to predict
#it's respective outcome 
xmt =  list(X.columns)
xmt.remove('size')
xmt.remove('trees')
xmt.remove('no.ramet')
xmt.remove('no.group')
xmt.remove('dist.isl.MF')
#xmt.remove('no.hab')

xmf =  list(X.columns)
xmf.remove('size')
xmf.remove('trees')
xmf.remove('no.ramet')
xmf.remove('no.group')
xmf.remove('dis.isl.MT')
#xmf.remove('no.hab')

Xmt = data[xmt]
Xmf = data[xmf]

xmt.append(outcome_mt)
dataMt = data[xmt]
xmf.append(outcome_mf)
dataMf = data[xmf]


# In[14]:


#Features for predicting the occurence of MT 

Xmt.head()


# In[15]:


Xmf.head()


# In[16]:


# Colinearity for predicting MT and MF separately
pearsoncorrXmt = Xmt.corr(method='pearson')
sb_plot = sb.heatmap(pearsoncorrXmt,xticklabels=pearsoncorrXmt.columns,yticklabels=pearsoncorrXmt.columns,cmap='RdBu_r',annot=False,linewidth=0.5)
plt.title("Correlation HeatMap for MT predictors")
plt.show()


# In[17]:


check_VIF(Xmt)


# In[70]:


sb.pairplot(data=Xmt)
plt.title("Scatter matrix after handling multicolinearity")
plt.show()


# In[18]:


pearsoncorrXmf = Xmf.corr(method='pearson')
sb_plot = sb.heatmap(pearsoncorrXmf,xticklabels=pearsoncorrXmf.columns,yticklabels=pearsoncorrXmf.columns,cmap='RdBu_r',annot=False,linewidth=0.5)
plt.title("Correlation HeatMap for MF predictors ")
plt.show()


# In[19]:


check_VIF(Xmf)


# In[531]:


sb.pairplot(data=Xmt)
plt.title("Scatter matrix after handling multicolinearity")
plt.show()


# In[20]:


# After taking care of multicollinearity, we proceed to model selection 
dataMt.head()


# In[21]:


dataMt=dataMt.rename(columns=lambda x: remove_dot(x))
dataMt.head()


# In[22]:


dataMf=dataMf.rename(columns=lambda x: remove_dot(x))
dataMf.head()


# In[23]:


mt_occurence_model = forward_selection(dataMt,remove_dot(outcome_mt))


# In[103]:


#Final Model parameters 
print(np.exp(mt_occurence_model.params))


# In[24]:


mf_occurence_model = forward_selection(dataMf,remove_dot(outcome_mf))


# In[25]:


#Final model parameters
print(np.exp(mf_occurence_model.params))


# ### Simulation of ML Estimators using pseudo observations of the  data 

# In[1078]:


#subsample from dataframe 
def subsample(dataFrame,sample_size,withRepeat=True):
    sample = dataFrame.loc[np.random.choice(dataFrame.index,sample_size, replace=withRepeat)]
    return sample
#to normalize a dataframe if needed
def normalize(df):
    normalized_df=(df-df.min())/(df.max()-df.min())
    return normalized_df


# In[1247]:


def plot_sim_results(df,title):
    k=1
    f, axs = plt.subplots(2,2,figsize=(15,6))
    for col in df.columns:
        i = list(df.columns).index(col)
        plt.subplot(1,len(df.columns), k)
        plt.hist(df[col])
        #plt.show()
        #plt.legend(loc=2)
        k+=1
    #for col in df.columns:
    #    par=norm.fit(df[col])
        #xs=df[col]
        #x = np.linspace(-2,2,1000)
        #x = np.linspace(min(df[col])-0.5, max(df[col])+0.5,1000)
        #fitted_df = norm.pdf(x,loc=par[0],scale=par[1])
        #plt.subplot(2,len(df.columns), k)
        #plt.plot(x,fitted_df,"red",label="Fitted normal dist",linestyle="dashed", linewidth=2)
        #k+=1      
    plt.suptitle(title)
    plt.show()


# In[1243]:


def simulation(data,start_sample_size=20,stop_sample_size=100,num_to_visualize=10):
    sample_sizes =[]
    MLEs = []
    #get first pseudo design matrix 
    sampled_data = subsample(data,start_sample_size)
    current_sample_size = start_sample_size
    
    #We will randomly pick j number of sample_sizes to visualize at that sample_size
    try:
        visualize = random.sample([x for x in range(start_sample_size,stop_sample_size,5)], k=num_to_visualize)
    except ValueError:
        num_to_visualize = 5
        visualize = random.sample([x for x in range(start_sample_size,stop_sample_size,5)], k=num_to_visualize)
    visualize.sort()
    
    
    #start generating and storing ML estimators
    
    while(True):
        if(current_sample_size > stop_sample_size):
            #stop when max sample_size is exceeded
            break 
        #get increment number 
        increment = random.choice([5,10]) #either by 5 or 10 randomly
        data_increment = subsample(data,increment) # dataset of increment number of rows to be added to old 
        #add the increment to the old pseudo design matrix to get the new one
        sampled_data = sampled_data.append(data_increment,ignore_index=True,sort=False)
        current_sample_size +=increment #reflect the increase the current_sample size 
        
        mle_k = [] #Empty list for  ML estimators for parameter k 
        for k in sampled_data.columns:
            mle = norm.fit(sampled_data[k]) #get ML estimates for this design matrix
            mle = tuple(map(lambda x: isinstance(x, float) and round(x, 2) or x, mle)) # round to 2 decimal places 
            mle_k.append(mle) #save for that parameter in list 
        MLEs.append(mle_k) # save the entire list after each iteration
        sample_sizes.append(current_sample_size) #just a record of the sample size used for this iteration
        
        if current_sample_size in visualize:
            
            #after iterations, arrange and sort the results into dataframes and visualize
            cols = data.columns
            k_dict ={}
            for col in cols:
                k = list(cols).index(col)
                k_dict[col]=[]
                for i in range(len(MLEs)):
                    k_vals = MLEs[i][k]
                    k_dict[col].append(k_vals)

            k_mus={}
            k_sigmas={}
            for k in k_dict:
                params = k_dict[k]
                mus=[]
                sigmas=[]
                for param in params:
                    mus.append(param[0])
                    sigmas.append(param[1])
                k_mus[k]=mus
                k_sigmas[k]=sigmas

            mu_dist_df = pd.DataFrame.from_dict(k_mus)
            sigma_dist_df = pd.DataFrame.from_dict(k_sigmas)
            title_mu = f"Simulation of Estimators for mu for {list(mu_dist_df.columns)} at sample size {current_sample_size}"
            title_sigma = f"Simulation Estimators for sigma for {list(sigma_dist_df.columns)} at sample size {current_sample_size}"


            plot_sim_results(mu_dist_df,title_mu)
            plot_sim_results(sigma_dist_df,title_sigma)
    
    return #mu_dist_df, sigma_dist_df,sample_sizes


# In[1147]:


mt_occurence_model.params


# In[1148]:


mf_occurence_model.params


# In[1149]:


#Designing the design matrix from the data
mt_model_parameters = list(dict(mt_occurence_model.params).keys())
mt_model_parameters.remove('Intercept')
mf_model_parameters = list(dict(mf_occurence_model.params).keys())
mf_model_parameters.remove('Intercept')
parameters_of_interest= set(mt_model_parameters+mf_model_parameters)
# Creating design matrix from the original data using only variables in model
original_design_mx = dataMt[parameters_of_interest] 


# In[1150]:


original_design_mx.head()


# In[1151]:


#Explore the distribution of those parameters 
for param in parameters_of_interest:
    plt.hist(original_design_mx[param],label=f"{param}")
    plt.legend(loc=1)
    plt.show()


# * We can log transform the heavily skewed variables to get more balance 

# * log-transforming dens_group and dist_group

# In[1153]:


plt.hist(np.log(original_design_mx['dens_group']),label="log-transformed dens_group")
plt.legend(loc=2)
plt.show()


# In[1154]:


plt.hist(np.log(original_design_mx['dist_group']),label="log-transformed dist_group")
plt.legend(loc=1)
plt.show()


# * No longer heavily skewed 

# In[1155]:


#Apply log transformation to the right parameters
original_design_mx['log_dens_group'] = np.log(original_design_mx['dens_group'])
original_design_mx['log_dist_group'] = np.log(original_design_mx['dist_group'])
original_design_mx.head()


# ###### We then fit a normal distribution for each parameter above : no_hab, log_dens_group, log_dist_group

# In[1156]:


#we then fit a normal distribution for this parameter
MLEstimates_no_hab = norm.fit(original_design_mx['no_hab'])
MLEstimates_no_hab


# In[1157]:


MLEstimates_log_dens_group = norm.fit(original_design_mx['log_dens_group'])
MLEstimates_log_dens_group


# In[1158]:


MLEstimates_log_dist_group = norm.fit(original_design_mx['log_dist_group'])
MLEstimates_log_dist_group


# In[1159]:


#Fitting a normal distribution for each parameter (or transformed parameter)


# In[1172]:


x = np.linspace(-10,10,100)
# Generate the pdf (fitted distribution)
fitted_pdf_no_hab = norm.pdf(x,loc = MLEstimates_no_hab[0],scale = MLEstimates_no_hab[1])
# Generate the pdf (normal distribution non fitted)
#normal_pdf = norm.pdf(x)

plt.plot(x,fitted_pdf_no_hab,"red",label="Fitted normal dist \n no_hab",linestyle="dashed", linewidth=2)
#plt.plot(x,normal_pdf,"blue",label="Normal dist", linewidth=2)
plt.title("Normal distribution fitting - Original Data")
plt.legend()
plt.show()


# In[1171]:


x = np.linspace(-10,10,100)
# Generate the pdf (fitted distribution)
fitted_pdf_log_dens_group = norm.pdf(x,loc = MLEstimates_log_dens_group[0],scale = MLEstimates_log_dens_group[1])
# Generate the pdf (normal distribution non fitted)
#normal_pdf = norm.pdf(x)

plt.plot(x,fitted_pdf_log_dens_group,"blue",label="Fitted normal \ndens_group",linestyle="dashed", linewidth=2)
#plt.plot(x,normal_pdf,"blue",label="Normal dist", linewidth=2)
plt.title("Normal Approximation - Original Data")
plt.legend()
plt.show()


# In[1173]:


x = np.linspace(-10,10,100)
# Generate the pdf (fitted distribution)
fitted_pdf_log_dist_group = norm.pdf(x,loc = MLEstimates_log_dist_group[0],scale = MLEstimates_log_dist_group[1])
# Generate the pdf (normal distribution non fitted)
#normal_pdf = norm.pdf(x)

plt.plot(x,fitted_pdf_log_dist_group,"green",label="Fitted normal dist \ndist_group",linestyle="dashed", linewidth=2)
#plt.plot(x,normal_pdf,"blue",label="Normal dist", linewidth=2)
plt.title("Normal Approximation - Original Data")
plt.legend()
plt.show()


# The ML Estimators for the unknown coefficients of the model are asymptotically normally distributed.  In a simulation study for different sample sizes, compare the actual(simulated) distribution of the estimators with the normal approximation for a special model (based on the observed data)
# 
# Choose for each sample size a fixed design matrix which consists of randomly selected rows of the real data matrix(possibly with repetition) and simulate for this desgin matrix several times pseudo observations of the target variable. 

# In[1209]:


new_design_matrix = original_design_mx[['no_hab','log_dens_group','log_dist_group']]
new_design_matrix.head()


# In[1210]:


start_sample_size = 20 
max_sample_size = 30
MLE_mu_sim_df, MLE_sigma_sim_df, sample_sizes = simulation(new_design_matrix,start_sample_size,max_sample_size)


# In[1251]:


start_sample_size = 20 
max_sample_size = 500
simulation(new_design_matrix,start_sample_size,max_sample_size)


# ### As the sample size increases, we see that the Estimators converge to the true Estimate and the distribution becomes more and more  normal
