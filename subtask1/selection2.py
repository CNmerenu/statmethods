import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
from rpy2.robjects.conversion import localconverter

R = robjects.r
R["source"]("backward_select.R")


def define_formula(formula):
    return R(formula)


def backward_select(island_dataframe, formula):
    """
    Returns the best model based on AIC score.
    """
    _backward_select = robjects.globalenv["backward_select"]
    with localconverter(robjects.default_converter + pandas2ri.converter):
        return _backward_select(df, formula)


mf_formula = define_formula(
    "formula <- mf.presence ~ 1 +"
    + "+".join(
        [
            "dist.isl.MF",
            "dist.group",
            "mean.height",
            "dist.land",
            "no.hab",
            "dens.ramet",
            "dens.group",
            "dens.ramet:dist.isl.MF",
            "dens.group:dist.isl.MF",
            "dens.ramet:dist.land",
            "dens.group:dist.land",
            "dist.land:dist.isl.MF",
            "no.hab:dens.group",
            "no.hab:dens.ramet",
        ]
    )
)

mt_formula = define_formula(
    "formula <- mt.presence ~ 1 +"
    + "+".join(
        [
            "dis.isl.MT",
            "dist.group",
            "mean.height",
            "dist.land",
            "no.hab",
            "dens.ramet",
            "dens.group",
            "dens.ramet:dis.isl.MT",
            "dens.group:dis.isl.MT",
            "dens.ramet:dist.land",
            "dens.group:dist.land",
            "dist.land:dis.isl.MT",
            "no.hab:dens.group",
            "no.hab:dens.ramet",
        ]
    )
)

if __name__ == "__main__":
    import pandas as pd

    df = pd.read_csv("islands.csv", encoding="iso-8859-15")
    model = backward_select(df, mt_formula)
    print(f"SELECTED MODEL: {model}")
