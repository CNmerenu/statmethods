import itertools
import math


class Formula:
    def __init__(self, target, *effects):
        self.target = target
        self.intersection = True
        self.main_effects = set(effects)
        self.interactions = set()

    def add_main_effect(self, effect):
        self.main_effects.add(effect)

    def remove_main_effect(self, effect):
        """
        Removes the given effect and all interactions with it from future consideration.
        """
        self.main_effects.discard(effect)
        self.remove_interactions_with(effect)

    def remove_interactions_with(self, effect):
        self.interactions = {
            e for e in self.interactions if e[0] != effect and e[1] != effect
        }

    def add_all_interactions(self):
        for interaction in itertools.combinations(self.main_effects, 2):
            self.add_interaction(*interaction)

    def add_interaction(self, effect1, effect2):
        assert (
            effect1 in self.main_effects
        ), f"{effect1} is not a main effect known to the model"
        assert (
            effect2 in self.main_effects
        ), f"{effect2} is not a main effect known to the model"
        effect1, effect2 = sorted((effect1, effect2))
        self.interactions.add((effect1, effect2))

    def remove_interaction(self, effect1, effect2):
        self.interactions.discard((effect1, effect2))
        self.interactions.discard((effect2, effect1))

    def __str__(self):
        assert self.main_effects, "No explanatory variables defined"

        interaction_effects = list(map(lambda x: f"{x[0]}:{x[1]}", self.interactions))

        formula = f"{self.target} ~ "

        if self.intersection:
            formula += "1"

        formula += " + "
        formula += " + ".join(self.main_effects)

        if interaction_effects:
            formula += " + "
            formula += " + ".join(interaction_effects)

        return formula


import pytest


def test_creation_is_possible_without_args():
    f = Formula("mt.presence")


def test_formula_forbids_interactions_of_non_main_effects():
    f = Formula("mt.presence", "dist")
    with pytest.raises(Exception):
        f.add_interaction("dist", "size")
    with pytest.raises(Exception):
        f.add_interaction("c", "dist")
    with pytest.raises(Exception):
        f.add_interaction("c", "d")


def test_uses_intersection_by_default():
    f = Formula("mt.presence", "dist")
    formula = str(f)
    assert "1" in formula


def test_ignores_intersection_when_requested():
    f = Formula("mt.presence", "size")
    f.intersection = False
    formula = str(f)
    assert "1" not in formula


def test_str_includes_initial_main_effects():
    main_effects = ["dist", "size", "c"]
    f = Formula("mt.presence", *main_effects)
    formula = str(f)
    for effect in main_effects:
        assert effect in formula


def test_str_includes_added_main_effects():
    main_effects = ["dist", "size"]
    f = Formula("mt.presence", *main_effects)
    f.add_main_effect("trees")
    formula = str(f)
    assert "trees" in formula


def test_str_includes_interactions():
    main_effects = ["dist", "size"]
    f = Formula("mt.presence", *main_effects)
    f.add_interaction("dist", "size")
    formula = str(f)
    assert "dist:size" in formula or "size:dist" in formula


def test_str_includes_interactions_only_once():
    main_effects = ["dist", "size"]
    f = Formula("mt.presence", *main_effects)

    # try to add them two times with different orders
    f.add_interaction("dist", "size")
    f.add_interaction("size", "dist")
    formula = str(f)

    assert ("dist:size" in formula and "size:dist" not in formula) or (
        "size:dist" in formula and "dist:size" not in formula
    )


def test_str_ignores_removed_main_effects():
    effect_to_remove = "trees"
    main_effects = ["dist", "size", effect_to_remove]
    f = Formula("mt.presence", *main_effects)
    f.remove_main_effect(effect_to_remove)
    formula = str(f)
    assert effect_to_remove not in formula


def test_str_ignores_removed_interactions():
    main_effects = ["dist", "size"]
    f = Formula("mt.presence", *main_effects)
    f.add_interaction("dist", "size")
    f.remove_interaction("size", "dist")  # even when removed other way round
    formula = str(f)
    assert f"size:dist" not in formula and f"dist:size" not in formula


def test_str_ignores_interactions_of_removed_main_effects():
    effect_to_remove = "size"
    main_effects = ["dist", effect_to_remove]
    f = Formula("mt.presence", *main_effects)
    f.add_interaction("dist", effect_to_remove)
    f.remove_main_effect(effect_to_remove)
    formula = str(f)
    assert (
        f"{effect_to_remove}:dist" not in formula
        and f"dist:{effect_to_remove}" not in formula
    )


def test_all_interactions_are_combinatorial():
    main_effects = ["dist", "size", "trees", "no.ramet"]
    for number_effects in range(2, len(main_effects)):
        f = Formula("mf.presence", *main_effects[:number_effects])
        f.add_all_interactions()
        formula = str(f)
        assert interactions_count(formula) == math.comb(number_effects, 2)


def interactions_count(formula):
    return formula.count(":")
