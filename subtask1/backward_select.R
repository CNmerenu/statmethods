backward_select <- function(df, formula) {
    island.glm <- glm (formula,
                       data=df,
                       family=binomial(link="logit")
                  )
    return(step(island.glm, direction="backward"))
}
